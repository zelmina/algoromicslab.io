#Include SequencingDataProcessor dependencies
#' @include standardGenerics.R MiodinWorkflowModule.R utilsSequencing.R
#' @include utilsDataProcessing.R
NULL

#' @title SequencingDataProcessor
#' 
#' @description
#' Workflow module class for processing RNA-seq counts.
#' 
#' @details
#' The modules checks the class of the assay to process and will recognize
#' child classes of \code{SummarizedExperiment}. A number of
#' processing steps are then carried out based on parameters supplied.
#' 
#' @section Input ports:
#' 
#' The module has a single input port:
#' \itemize{
#'   \item \code{dataset}: This contains two slots, \code{mds} and
#'   \code{assayName}, which contain the \code{MiodinDataset} and assay name,
#'   respectively.
#' }
#' 
#' @section Output ports:
#' 
#' The module has a single output port:
#' \itemize{
#'   \item \code{dataset}: This contains two slots, \code{mds} and
#'   \code{assayName}, which contain the \code{MiodinDataset} and assay name,
#'   respectively.
#' }
#' 
#' @author Benjamin Ulfenborg
#' 
#' @seealso \link{MiodinWorkflowModule-class}
#' 
#' @references Forthcoming
#' 
#' @name WorkflowModule-SequencingDataProcessor
#' @export SequencingDataProcessor
SequencingDataProcessor <- setClass(
  "SequencingDataProcessor",
  prototype = list(
    parameterValues = list(
      postQC = TRUE,
      filterLowCount = FALSE,
      filterLowCountThresh = 5,
      filterLowCountNumObs = 1,
      filterLowVar = FALSE,
      filterLowVarThresh = 0.1,
      contrastName = NULL,
      repCollapse = NULL,
      stabilizeVariance = FALSE,
      stabilizeVarianceMethod = "rlog"
    ),
    documentation = "",
    instantiator = "processSequencingData"
  ),
  validity = function(object){
    errors <- character()
    if(length(errors) == 0)
      return(TRUE)
    else
      return(errors)
  },
  contains = "MiodinWorkflowModule",
  sealed = TRUE
)

#' @export
setMethod("initialize", "SequencingDataProcessor",
          function(.Object, ...){
            iPort = MiodinDataPort()
            iPort <- setProp(iPort, list(
              slots = list(
                mds = "MiodinDataset", assayName = "character"
              ),
              slotDesc = list(
                mds = "MiodinDataset containing count data",
                assayName = "Name of assay to process"
              ))
            )
            oPort <- MiodinDataPort()
            oPort <- setProp(oPort, list(
              slots = list(
                mds = "MiodinDataset", assayName = "character"
              ),
              slotDesc = list(
                mds = "MiodinDataset where processed data is stored",
                assayName = "Name of processed assay"
              ))
            )
            .Object <- setProp(.Object, list(
              inputPorts = list(dataset = iPort),
              outputPorts = list(dataset = oPort),
              parameterDesc = list(
                General = data.frame(
                  Parameter = c("postQC", "contrastName"),
                  Data = c("Logical", "Character"),
                  Mandatory = rep("No", 2),
                  Description = c("Whether or not to perform post-processing QC",
                                  "Name of study contrast to use for coloring QC plots")),
                RNA = data.frame(
                  Parameter = c("filterLowCount",
                                "filterLowCountThresh", "filterLowCountNumObs",
                                "filterLowVar", "filterLowVarThresh",
                                "repCollapse", "stabilizeVariance",
                                "stabilizeVariance"),
                  Data = c("Logical", "Numeric", "Numeric",
                           "Logical", "Numeric", "Character",
                           "Logical", "Character"),
                  Mandatory = rep("No", 8),
                  Description = c("Whether or not to filter low-expressed genes",
                                  "Threshold below which genes are not expressed",
                                  "Number of observations for which a gene must be expressed",
                                  "Whether or not to filter low-varance genes",
                                  "Coefficient of variation threshold below which genes do no vary",
                                  "Vector of sample table column names used for replicate collapse",
                                  "Whether or not to perform variance stabilization",
                                  "Method of transformation"))
              )
            ))
            return(.Object)
          } )

setMethod("execute", "SequencingDataProcessor",
          function(object, studies, projectPath, workflowConfiguration,
                   inputPorts, postQC, filterLowCount, filterLowCountThresh,
                   filterLowCountNumObs, filterLowVar, filterLowVarThresh,
                   contrastName, repCollapse, stabilizeVariance,
                   stabilizeVarianceMethod, verbose){

            #Process input port
            miodinDataset <- inputPorts[["dataset"]][["mds"]]
            assayName <- inputPorts[["dataset"]][["assayName"]]
            if(class(miodinDataset) != "MiodinDataset")
              stop("Dataset must be a MiodinDataset")
            if(class(assayName) != "character")
              stop("Name must be character")
            if(!(assayName %in% names(getAll(miodinDataset, "assayData"))))
              stop("Specified assayName not found in dataset")

            #Retrieve contrast vector
            sampleTable <- as(get(miodinDataset, "phenoData", assayName),
                              "data.frame")
            studyName <- get(miodinDataset, "study", assayName)
            contrastVector <- utilMakeContrastVector(studies, studyName,
                                                     contrastName, sampleTable)

            #Check assay class
            assay <- miodinDataset[[assayName]]
            if(inherits(assay, "SummarizedExperiment")){
              miodinDataset <- utilProcessorSequencingRNA(
                miodinDataset = miodinDataset,
                assayName = assayName,
                sampleTable = sampleTable,
                projectPath = projectPath,
                sampleGroups = contrastVector,
                contrastName = contrastName,
                filterLowCount = filterLowCount,
                filterLowCountThresh = filterLowCountThresh,
                filterLowCountNumObs = filterLowCountNumObs,
                filterLowVar = filterLowVar,
                filterLowVarThresh = filterLowVarThresh,
                repCollapse = repCollapse,
                stabilizeVariance = stabilizeVariance,
                stabilizeVarianceMethod = stabilizeVarianceMethod,
                verbose = verbose)
              if(postQC)
                miodinDataset <- utilQualitySequencingRNA(
                  miodinDataset = miodinDataset,
                  assayName = assayName,
                  qcName = "Processed data",
                  contrastName = contrastName,
                  studies = studies,
                  verbose = verbose)
            } else{
              stop("Unknown assay of class ", class(assay))
            }
            
            #Return output ports
            return(list(
              dataset = list(
                mds = miodinDataset, assayName = assayName
              )
            ))
          } )

#' @title processSequencingData
#' 
#' @description
#' Instatiates a \code{SequencingDataProcessor} module in a workflow.
#' 
#' @param name Name of the workflow module (character string).
#' @param postQC Whether or not to perform post-processing QC.
#' @param contrastName Name of study contrast to use for coloring QC plots.
#' @param filterLowCount Whether or not to filter low-count genes.
#' @param filterLowCountThresh Threshold below which genes are not expressed.
#' @param filterLowCountNumObs Number of observations for which a gene must be expressed.
#' @param filterLowVar Whether or not to filter low-varance genes.
#' @param filterLowVarThresh Coefficient of variation threshold below which genes do no vary.
#' @param repCollapse Vector of sample table column names used for replicate collapse.
#' @param stabilizeVariance Whether or not to stabilize variance.
#' @param stabilizeVarianceMethod Method of transformation, either \code{"rlog"}
#' or \code{"vst"}
#' 
#' @author Benjamin Ulfenborg
#' 
#' @seealso \link{WorkflowModule-SequencingDataProcessor}
#' 
#' @references Forthcoming
#' 
#' @name InstatiateModule-processSequencingData
#' @export processSequencingData
processSequencingData <- function(name,
                                  postQC = TRUE,
                                  contrastName = NULL,
                                  filterLowCount = FALSE,
                                  filterLowCountThresh = 5,
                                  filterLowCountNumObs = 1,
                                  filterLowVar = FALSE,
                                  filterLowVarThresh = 0.1,
                                  repCollapse = NULL,
                                  stabilizeVariance = FALSE,
                                  stabilizeVarianceMethod = "rlog"){
  mwm <- workflowModule("SequencingDataProcessor", name,
                        postQC = postQC,
                        contrastName = contrastName,
                        filterLowCount = filterLowCount,
                        filterLowCountThresh = filterLowCountThresh,
                        filterLowCountNumObs = filterLowCountNumObs,
                        filterLowVar = filterLowVar,
                        filterLowVarThresh = filterLowVarThresh,
                        repCollapse = repCollapse,
                        stabilizeVariance = stabilizeVariance,
                        stabilizeVarianceMethod = stabilizeVarianceMethod)
  return(mwm)
}

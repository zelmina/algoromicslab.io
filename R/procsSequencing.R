#' @include utilsDataProcessing.R
NULL

#' @title procFilterLowCounts
#' 
#' @description
#' Processing function to filter features with low counts.
#' 
#' @param filterLowCountThresh Threshold below which features are not expressed.
#' @param filterLowCountNumObs Number of observations for which a feature must
#' be expressed.
#' 
#' @return
#' A data processing environment.
#' 
#' @author Benjamin Ulfenborg
#' 
#' @references Forthcoming
#' 
#' @export
procFilterLowCounts <- function(filterLowCountThresh, filterLowCountNumObs){
  procFun <- function(data){
    if(is(data, "SummarizedExperiment"))
      dataMat <- SummarizedExperiment::assay(data)
    else if(is(data, "MSnSet"))
      dataMat <- exprs(data)
    else{
      warning("Unable to filter low-counts on data object", class(data))
      return(data)
    }
    keep <- apply(dataMat, 1, function(x) {
      sum(x > filterLowCountThresh) > filterLowCountNumObs
    } )
    return(data[keep, ])
  }
  procEnv <- utilMakeProcEnv(desc = "Filtering out low-count features",
                             procName = "procFilterLowCounts",
                             procPackage = "miodin",
                             procVersion = packageVersion("miodin"),
                             procFun = procFun,
                             lowExpThresh = filterLowCountThresh,
                             lowExpNumObs = filterLowCountNumObs)
  return(procEnv)
}

#' @title procVarianceStabilization
#' 
#' @description
#' Processing function to stabilize variance with the \code{DESeq2} package.
#' 
#' @param method Method of transformation, either \code{"rlog"} or
#' \code{"vst"}.
#' 
#' @return
#' A data processing environment.
#' 
#' @author Benjamin Ulfenborg
#' 
#' @references Forthcoming
#' 
#' @export
procVarianceStabilization <- function(method = "rlog"){
  procFun <- function(data){
    if(!(method %in% c("rlog", "vst")))
      stop("Unknown transformation ", method)
    if(method == "rlog") ##
      transMatrix <- DESeq2::rlog(SummarizedExperiment::assay(data))
    else if(method == "vst")
      transMatrix <- DESeq2::vst(SummarizedExperiment::assay(data))
    colnames(data) <- SummarizedExperiment::colData(data)$SampleName
    colnames(transMatrix) <- SummarizedExperiment::colData(data)$SampleName
    if(!is.matrix(transMatrix))
      transMatrix <- SummarizedExperiment::assay(transMatrix)
    SummarizedExperiment::assays(data)$trans <- transMatrix
    return(data)
  }
  procEnv <- utilMakeProcEnv(desc = "Performing variance stabilization",
                             procName = "procVarianceStabilization",
                             procPackage = "miodin",
                             procVersion = packageVersion("miodin"),
                             procFun = procFun)
  return(procEnv)
}

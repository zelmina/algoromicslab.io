#Include DebugAnalyzer dependencies
#' @include standardGenerics.R MiodinWorkflowModule.R
NULL

#' @title DebugAnalyzer
#' 
#' @description
#' A workflow module used to test statisitcal testing functionality.
#' 
#' @param test What statistical test to perform.
#' @param varEqual Logical indicating whether group variances are assumed to be
#' equal.
#' @param contrast Name of contrast to test.
#' 
#' @details
#' 
#' This module is used in the debug workflow to perform a statistical test
#' on the assay data processed by \code{DebugProcessor}.
#' 
#' @author Benjamin Ulfenborg
#' 
#' @seealso \link{MiodinWorkflowModule-class}
#' 
#' @references Forthcoming
#' 
#' @name WorkflowModule-DebugAnalyzer
#' @export DebugAnalyzer
DebugAnalyzer <- setClass(
  "DebugAnalyzer",
  prototype = list(
    parameterValues = list(
      test = "ttest",
      varEqual = FALSE,
      contrast = ""
    ),
    documentation = "A module used to perform t-test on debug data",
    tags = c("Statistics", "Debug")
  ),
  validity = function(object){
    errors <- character()
    if(length(errors) == 0)
      return(TRUE)
    else
      return(errors)
  },
  contains = "MiodinWorkflowModule",
  sealed = TRUE
)

#' @export
setMethod("initialize", "DebugAnalyzer",
          function(.Object, ...){
            iPort = MiodinDataPort()
            iPort <- setProp(iPort, list(
              slots = list(
                processedDataset = "MiodinDataset", assayName = "character"
              ),
              slotDesc = list(
                processedDataset = "MiodinDataset where processed data is stored",
                assayName = "Name of imported assay (an assay table in a study)"
              ))
            )
            oPort <- MiodinDataPort()
            oPort <- setProp(oPort, list(
              slots = list(
                result = "MiodinAnalysisResult"
              ),
              slotDesc = list(
                result = "MiodinAnalysisResult containing analysis result"
              ))
            )
            .Object <- setProp(.Object, list(
              inputPorts = list(dataset = iPort),
              outputPorts = list(analysisResult = oPort),
              parameterDesc = list(Test = data.frame(
                Parameter = c("test", "varEqual", "contrast"),
                Data = c("Character", "Logical", "Character"),
                Mandatory = c("No", "No", "Yes"),
                Description = c("What statistical test to perform.",
                                "Logical indicating whether group variances are assumed to be equal.",
                                "Name of contrast to test."))
              )
            ))
            return(.Object)
          } )

setMethod("execute", "DebugAnalyzer",
          function(object, studies, projectPath, workflowConfiguration,
                   inputPorts, verbose, test, varEqual, contrast){
            
            #Validate arguments
            if(!(test %in% c("ttest")))
              stop("test must be ttest")
            if(!is.logical(varEqual))
              stop("varEqual must be logical")
            if(contrast == "")
              stop("contrast must be specified")
            
            #Process input port
            dataset <- inputPorts[["dataset"]][["processedDataset"]]
            datasetName <- getProp(dataset, "name")[["name"]]
            assayName <- inputPorts[["dataset"]][["assayName"]]
            if(class(dataset) != "MiodinDataset")
              stop("Dataset must be a MiodinDataset")
            if(class(assayName) != "character")
              stop("Name must be character")
            assayData <- getAll(dataset, "assayData", processing = FALSE)
            if(!(assayName %in% names(assayData)))
              stop("Specified assayName not found in dataset")
            datasetStudies <- getAll(dataset, "study")
            studyName <- datasetStudies[[assayName]]
            if(!(studyName %in% names(studies)))
              stop("Study ", studyName, " does not exist")
            contrastList <- getAll(studies[[studyName]], "contrast")
            if(!(contrast %in% names(contrastList)))
              stop("Contrast ", contrast, " does not exist in study ", studyName)

            #Retrieve contrast and index dataset
            sampleTable <- getProp(studies[[studyName]], "sampleTable",
                                   props = "table")[["table"]]
            contrastVector <- utilMakeContrastVector(studies, studyName,
                                                     contrast, sampleTable)
            eset <- get(dataset, "assayData", name = assayName)
            dropInd <- which(!(names(contrastVector) %in% colnames(eset)))
            if(length(dropInd) > 0){
              warning("  Samples in contrast missing in dataset, dropping samples in contrast")
              contrastVector <- contrastVector[-dropInd]
            }
            eset <- eset[, names(contrastVector)]

            #Validate contrast vector
            if(length(levels(contrastVector)) != 2)
               stop("Contrast does not have two groups")
            groupSizeOk <- sapply(levels(contrastVector), function(x) {
              numSamples <- length(contrastVector[contrastVector == x])
              ifelse(numSamples < 2, FALSE, TRUE)
            } )
            if(!all(groupSizeOk))
              stop("There must be at least two samples in each group")

            #Perform test
            pairedness <- ifelse(getProp(contrastList[[contrast]], "paired")[["paired"]] == "",
                            FALSE, TRUE)     
            if(test == "ttest"){
              result <- apply(Biobase::exprs(eset), 1, function(x) {
                res <- t.test(x ~ contrastVector, var.equal = varEqual, paired = pairedness)
                return(res$p.value)
              } )
              result <- as.data.frame(result)
              rownames(result) <- rownames(eset)
              colnames(result) <- c("P-value")
              mar <- MiodinAnalysisResult(datasetName)
              mar <- add(mar, "result", list("ttest p-values" = result))
            }
            
            #Return output ports
            return(list(analysisResult = list(result = mar)))
          } )

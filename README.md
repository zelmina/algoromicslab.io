Miodin: An R package for multi-omics data integration
=====================================================

The `miodin` package provides a software infrastructure for declaring study designs and executing data analysis workflows in R. The primary audience for the package are those who want to perform multi-omics data integration, either across studies on the same set of variables or across different omics modalities on the same samples. An important design goal was to render data analysis as streamlined as possible, to make data integration easy, and promote transparent and reproducible biomedical data science.

More in-depth description of how to the `miodin` package can be found in the [user manual](https://algoromics.gitlab.io). For complete description of classes and method, see the [reference documentation](https://gitlab.com/algoromics/algoromics.gitlab.io/blob/master/miodin.pdf).

News
====

\[21/9 2018\] Version 0.1.0 released

Installation
============

The easiest way to install `miodin` is to run the following code in R.

``` r
source("https://bioconductor.org/biocLite.R")
biocLite(c('MultiDataSet', 'Biobase', 'BiocGenerics', 'oligo', 'snpStats',
'DMRcatedata', 'ArrayExpress', 'AffyCompatible', 'crlmm', 'limma', 'minfi',
'SNPRelate', 'wateRmelon', 'DMRcate', 'IRanges', 'SummarizedExperiment',
'GenomicRanges', 'DESeq2', 'MSnbase', 'edgeR'))
if(!require(devtools)) install.packages("devtools")
devtools::install_git(url = "https://gitlab.com/algoromics/miodin.git")
```

Citation
========

Reference forthcoming

Contact and support
========

Questions can be sent to the package maintainer Benjamin Ulfenborg (<benjamin.ulfenborg@his.se>). For user support, bugs and suggestions on improving the package, you can also use the repository [Issue Tracker](https://gitlab.com/algoromics/algoromics.gitlab.io/issues).
